import React, { useState } from "react";
import { View, StyleSheet, Text } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import InfoBlock from "./InfoBlock";
import CardList from "./CardList";

function SearchForecastOnSevenDay(props) {
  const [city, setCity] = useState();

  function showWeatherForecastOnSevenDays(i, value) {
    props.getWeatherForecastOnSevenDays(value);
    setCity(value);
  }

  return (
    <View style={styles.forecast}>
      <Text style={styles.title}>7 Days Forecast</Text>
      <ModalDropdown 
        style={styles.select} 
        options={['Самара', 'Тольятти', 'Саратов', 'Казань', 'Краснодар']} 
        defaultValue='Select city' 
        onSelect={showWeatherForecastOnSevenDays} 
        textStyle={styles.text} 
        dropdownStyle={styles.drop}
      />
      {!props.isFormSevenDaysSubmited && <InfoBlock />}
      {props.isFormSevenDaysSubmited &&
        <CardList
          cards={props.cards}
          forecast={props.forecast}
          showNextCard={props.showNextCard}
          showPreviousCard={props.showPreviousCard}
          numberOfInitalCard={props.numberOfInitalCard}
          isLeftButtonEnabled={props.isLeftButtonEnabled}
          isRightButtonEnabled={props.isRightButtonEnabled}
        />
      }
    </View>
  );
}

export default SearchForecastOnSevenDay;


const styles = StyleSheet.create({
  forecast: {
    flex: 1,
    marginHorizontal: 10,
    marginTop: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginBottom: 60,
    padding: 50,
    paddingTop: 0,
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 32,
    color: '#2c2d76',
    marginTop: 50,
  },
  select: {
    width: '100%',
    height: 30,
    marginTop: 32,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#8083a4',
    paddingLeft: 10,
  },
  text: {
    fontSize: 16,
  },
  drop: {
    width: '60%',
    alignSelf: 'flex-start',
    marginLeft: 0,
  }
});
