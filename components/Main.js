import React from "react";
import SearchForecastOnSevenDay from "./SearchForecastOnSevenDay";
import SearchForecastOnHistoryDay from "./SearchForecastOnHistoryDay";
import { View, StyleSheet } from 'react-native';

function Main(props) {
  return (
    <View style={styles.main}>
      <SearchForecastOnSevenDay
        forecast="7days"
        cards={props.cardsSevenDays}
        getWeatherForecastOnSevenDays={props.getWeatherForecastOnSevenDays}
        showNextCard={props.showNextCard}
        showPreviousCard={props.showPreviousCard}
        numberOfInitalCard={props.numberOfInitalCard}
        isLeftButtonEnabled={props.isLeftButtonEnabled}
        isRightButtonEnabled={props.isRightButtonEnabled}
        isFormSevenDaysSubmited={props.isFormSevenDaysSubmited}
      />
      <SearchForecastOnHistoryDay
        forecast="day"
        cards={props.cardDayInPast}
        getHistoricalWeatherData={props.getHistoricalWeatherData}
        isFormHistoryDaySubmited={props.isFormHistoryDaySubmited}
      />
    </View>
  );
}

export default Main;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    marginTop: 10,
  },
});