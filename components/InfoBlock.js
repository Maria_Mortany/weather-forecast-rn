import React from "react";
import { View, StyleSheet, Text, Image } from 'react-native';
import CloudSvg from './CloudSvg';

function InfoBlock() {
    return (
        <View style={styles.container}>
            <CloudSvg />
            <Text style={styles.text}>
                Fill in all the fields and the weather will be displayed
            </Text>
        </View>
    );
}

export default InfoBlock;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 64,
        flexDirection: 'column',
        alignItems: 'center',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#8083a4',
        marginTop: 20,
        marginBottom: 3,
        alignSelf: 'center'
    }
});
