import React, { useMemo } from "react";
import { View, StyleSheet, Text, ImageBackground } from 'react-native';

function Card(props) {
    console.warn(props);
    const temperatureCard = useMemo(() => {
        if (props.card.temperature > 0) {
            return "+" + props.card.temperature;
        } else if (props.card.temperature < 0) {
            return "-" + props.card.temperature;
        } else {
            return String(props.card.temperature);
        }
    }, [props.card]);

    return (
        <View style={styles.card}>
            <ImageBackground source={{ uri: `${props.card.iconUrl}` }} style={styles.image} style={props.forecast==='7days' ? {padding: 10}: {padding: 40}} >
                <Text style={styles.date}>{props.card.date}</Text>
                <Text style={styles.temperature}>{temperatureCard}&deg;</Text>
            </ImageBackground>
        </View>
    );
}

export default Card;

const styles = StyleSheet.create({
    card: {
        flex: 1,
        backgroundColor: '#373af5',
        borderWidth: 2,
        borderRadius: 8,
        marginRight: 10,
        marginTop: 50,
        padding: 10,
    },
    image: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    date: {
        width: 120,
        height: 120,
        marginLeft: 20,
        fontWeight: 'bold',
        fontSize: 16,
        color: "#fff"
    },
    temperature: {
        fontWeight: 'bold',
        fontSize: 32,
        marginTop: 60,
        marginRight: 10,
        alignSelf: 'flex-end',
        color: "#fff"
    }
});
