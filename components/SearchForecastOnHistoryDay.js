import React, { useState } from "react";
import { View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { DateTimePickerModal } from "react-native-modal-datetime-picker";
import InfoBlock from "./InfoBlock";
import CardList from "./CardList";

function SearchForecastOnHistoryDay(props) {
    const [city, setCity] = useState("");
    const [date, setDate] = useState('Select date');

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    function showWeatherForecastOnHistoryDay(city, date) {
        if (city !== "" && date !== "Select date") {
            const newDate = new Date(date);
            const dateInThePast =
                Date.UTC(
                    newDate.getUTCFullYear(),
                    newDate.getUTCMonth(),
                    newDate.getUTCDate(),
                    newDate.getUTCHours(),
                    newDate.getUTCMinutes(),
                    newDate.getUTCSeconds()
                ) / 1000;
            props.getHistoricalWeatherData(city, dateInThePast);
        }
    }

    function onSelect(i, value) {
        setCity(value);
        showWeatherForecastOnHistoryDay(value, date);
    }

    const handleDate = (date) => {
        setDate(date.toLocaleDateString());
        showWeatherForecastOnHistoryDay(city, date.toLocaleDateString());
        hideDatePicker();
    };



    return (
        <View style={styles.forecast}>
            <Text style={styles.title}>Forecast for a Date in the Past</Text>
            <ModalDropdown
                style={styles.select}
                options={['Самара', 'Тольятти', 'Саратов', 'Казань', 'Краснодар']}
                defaultValue='Select city'
                onSelect={onSelect}
                textStyle={styles.text}
                dropdownStyle={styles.drop}
            />
            <View style={styles.container}>
                <TextInput style={styles.date} defaultValue='Select date' editable={false} value={date} />
                <TouchableOpacity
                    style={styles.button}
                    onPress={showDatePicker}
                >
                    <Text style={{color: "#fff"}}>&#9660;</Text>
                </TouchableOpacity>
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleDate}
                    onCancel={hideDatePicker}
                />
            </View>

             {!props.isFormHistoryDaySubmited && <InfoBlock />}
      {props.forecast === "day" && props.isFormHistoryDaySubmited &&
        <CardList
          cards={props.cards}
          forecast={props.forecast}
          showNextCard={props.showNextCard}
          showPreviousCard={props.showPreviousCard}
          numberOfInitalCard={props.numberOfInitalCard}
          isLeftButtonEnabled={props.isLeftButtonEnabled}
          isRightButtonEnabled={props.isRightButtonEnabled}
        />
      }
        </View>
    );
}

export default SearchForecastOnHistoryDay;


const styles = StyleSheet.create({
    forecast: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
        marginBottom: 60,
        padding: 50,
        paddingTop: 0,
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 32,
        color: '#2c2d76',
        marginTop: 50,
    },
    select: {
        width: '100%',
        height: 30,
        marginTop: 32,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#8083a4',
        paddingLeft: 10,
    },
    text: {
        fontSize: 16,
    },
    drop: {
        width: '60%',
        alignSelf: 'flex-start',
        marginLeft: 0,
    },
    container: {
        flex: 1,
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    date: {
        width: '90%',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#8083a4',
        paddingLeft: 10,
    },
    button: {
        alignItems: 'center',
        backgroundColor: "#373af5",
        width: '10%',
        height: 30,
        borderColor: "#FFFFFF",
        borderWidth: 2,
        borderRadius: 20
    },
});