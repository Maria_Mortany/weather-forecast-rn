import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

function Footer() {
  return (
      <View style={styles.footer}>
        <Text style={styles.discription}>C Любовью от Mercury Development</Text>
      </View>
  );
}

export default Footer;

const styles = StyleSheet.create({
  footer: {
    marginTop: 78,
    marginBottom: 16
  },
  discription: {
    fontSize: 14,
    color: '#fff',
    textTransform: 'uppercase',
    opacity: 0.6,
  },
});
