import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

function Header() {
  return (
      <View style={styles.header}>
        <Text style={styles.title}>Weather<Text>{'\n'}forecast</Text></Text>
      </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  header: {
    margin: 65,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 32,
    color: '#fff',
  },
});
