import Card from "./Card";
import React from "react";
import { ScrollView, StyleSheet } from 'react-native';

function CardList(props) {


    function renderCard({card}) {
        <Card  card={card} forecast={props.forecast} />
    } 


  return (
    <ScrollView horizontal={true} style={styles.container}>
      {props.cards
          .map((card, i) => (
            <Card key={i} card={card} forecast={props.forecast} />
        ))}
    </ScrollView>
  );
}

export default CardList;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 24,
        flexDirection: 'row',
    },
    cards: {
        flex: 1,
        flexDirection: 'row',
        overflow: 'scroll',
    }
});
