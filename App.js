import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from "react";
import { StyleSheet, ScrollView , ImageBackground } from 'react-native';
import { cos } from 'react-native-reanimated';
import Header from "./components/Header";
import Footer from "./components/Footer";
import Main from "./components/Main";
import apiWeather from "./utils/WeatherApi";
import imageButton from "./images/Bg-buttom.png";
import imageUp from "./images/Bg-up.png";


export default function App() {
  const [cardsSevenDays, setCardsSevenDays] = useState([]);
  const [cardDayInPast, setCardDayInPast] = useState([]);
  const [numberOfInitalCard, setNumberOfInitalCard] = useState(0);
  const [isRightButtonEnabled, setIsRightButtonEnabled] = useState(true);
  const [isLeftButtonEnabled, setIsLeftButtonEnabled] = useState(false);
  const [isFormSevenDaysSubmited, setIsFormSevenDaysSubmited] = useState(false);
  const [isFormHistoryDaySubmited, setIsFormHistoryDaySubmited] = useState(false);


  const getCoordinates = (city) => {
    let cordinates;

    switch (city) {
      case "Самара":
        cordinates = {
          lat: 53.195873,
          lon: 50.100193,
        };
        break;
      case "Тольятти":
        cordinates = {
          lat: 53.507836,
          lon: 49.420393,
        };
        break;
      case "Саратов":
        cordinates = {
          lat: 51.533557,
          lon: 46.034257,
        };
        break;
      case "Казань":
        cordinates = {
          lat: 55.796127,
          lon: 49.106405,
        };
        break;
      case "Краснодар":
        cordinates = {
          lat: 45.03547,
          lon: 38.975313,
        };
        break;
      default:
        console.log("Нет такого города");
    }

    return cordinates;
  };

  const optionsData = {
    day: "numeric",
    month: "short",
    year: "numeric",
  };

  function changeDateFormat(date) {
    return new Date(date * 1000)
      .toLocaleDateString("en-GB", optionsData)
      .toLowerCase();
  }

  function createCard(date, temperature, iconUrl) {
    const dateCard= changeDateFormat(date);
    const newCardItem = {
      date: dateCard,
      temperature: Math.round(temperature),
      iconUrl: `http://openweathermap.org/img/wn/${iconUrl}@2x.png`,
    };
    return newCardItem;
  }

  function getWeatherForecastOnSevenDays(city) {
    const cordinatesCity = getCoordinates(city);

    setIsFormSevenDaysSubmited(false);

    apiWeather
      .getWeatherForecastOnSevenDays(cordinatesCity)
      .then((data) => {
        setIsFormSevenDaysSubmited(true);
        const newCards = data.daily.map((item) => {
          return createCard(item.dt, item.temp.day, item.weather[0].icon);
        });

        setCardsSevenDays(newCards);
        setNumberOfInitalCard(0);
        setIsRightButtonEnabled(true);
        setIsLeftButtonEnabled(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function getHistoricalWeatherData(city, date) {
    const coordinatesCity = getCoordinates(city);

    setIsFormHistoryDaySubmited(false);

    apiWeather
      .getWeatherForecastOnDateinThePast(coordinatesCity, date)
      .then((data) => {
        setIsFormHistoryDaySubmited(true);
        const newCard = createCard(
          data.current.dt,
          data.current.temp,
          data.current.weather[0].icon
        );
        setCardDayInPast([newCard, ...cardDayInPast]);
        setNumberOfInitalCard(0);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function showNextCard() {
    if (numberOfInitalCard < cardsSevenDays.length - 3) {
      setNumberOfInitalCard(numberOfInitalCard + 1);
    }
  }

  function showPreviousCard() {
    if (numberOfInitalCard !== 0) {
      setNumberOfInitalCard(numberOfInitalCard - 1);
    }
  }

  useEffect(() => {
    if (numberOfInitalCard === cardsSevenDays.length - 3) {
      setIsRightButtonEnabled(false);
    }
    if (numberOfInitalCard === cardsSevenDays.length - 4) {
      setIsRightButtonEnabled(true);
    }
    if (numberOfInitalCard === 1) {
      setIsLeftButtonEnabled(true);
    }
    if (numberOfInitalCard === 0) {
      setIsLeftButtonEnabled(false);
    }
  }, [cardsSevenDays.length, numberOfInitalCard]);

  return (
    <ScrollView style={styles.container}>
     <ImageBackground source={imageUp} style={styles.image} >
        <ImageBackground source={imageButton} style={styles.image} style={{flex: 1, alignItems: 'center', justifyContent: 'center',}}>
          <StatusBar style="auto" />
          <Header />
            <Main 
              cardsSevenDays={cardsSevenDays}
              cardDayInPast={cardDayInPast}
              getWeatherForecastOnSevenDays={getWeatherForecastOnSevenDays}
              showNextCard={showNextCard}
              showPreviousCard={showPreviousCard}
              numberOfInitalCard={numberOfInitalCard}
              isLeftButtonEnabled={isLeftButtonEnabled}
              isRightButtonEnabled={isRightButtonEnabled}
              getHistoricalWeatherData={getHistoricalWeatherData}
              isFormSevenDaysSubmited={isFormSevenDaysSubmited}
              isFormHistoryDaySubmited={isFormHistoryDaySubmited}
            />
            <Footer /> 
        </ImageBackground>
     </ImageBackground>
    </ScrollView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#373af5',
    resizeMode: 'contain',
  },
  text: {
    color: '#fff',
    fontSize: 18,
  }
});
